extends Control

export (String, MULTILINE) var help_text:String

func _ready() -> void:
	$Show.visible = false
	$Hide.visible = true

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("help"):
		$Show.visible = !$Show.visible
		$Hide.visible = !$Hide.visible
		$Show/Label2.text = "EXIT\n- Escape\n\n" + help_text
	if Input.is_action_just_pressed("exit"):
		get_tree().call_deferred("quit")