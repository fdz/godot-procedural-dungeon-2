extends Node2D

var DRAW = true
var DRAW_GRID = true
var DRAW_ZONE = true
var DRAW_ROOMS = true

var dungeon:TileMap
var zone:Rect2

func _process(delta):
	update()

func _draw():
#	if !DRAW or dungeon.yield_generate == null:
	if !DRAW:
		return
	var zone = Rect2()
	if dungeon.is_generating() || self.zone.position == Vector2.ZERO && self.zone.size == Vector2.ZERO:
		zone = dungeon.zone
	else:
		zone = self.zone
	if DRAW_GRID:
		var color = Color(0.2, 0.2, 0.2, 1)
		var from
		var to
		for y in zone.size.y:
			from = dungeon.map_to_world(zone.position + Vector2.DOWN * y)
			to = from + Vector2.RIGHT * zone.size.x * dungeon.cell_size.x
			draw_line(from, to, color)
		from = dungeon.map_to_world(zone.position + Vector2.DOWN * zone.size.y)
		to = from + Vector2.RIGHT * zone.size.x * dungeon.cell_size.x
		draw_line(from, to, color)
		for x in zone.size.x:
			from = dungeon.map_to_world(zone.position + Vector2.RIGHT * x)
			to = from + Vector2.DOWN * zone.size.y * dungeon.cell_size.y
			draw_line(from, to, color)
		from = dungeon.map_to_world(zone.position + Vector2.RIGHT * zone.size.x)
		to = from + Vector2.DOWN * zone.size.y * dungeon.cell_size.y
		draw_line(from, to, color)
	if DRAW_ZONE:
		var left_up = dungeon.map_to_world(zone.position)
		var left_down = dungeon.map_to_world(zone.position + Vector2.DOWN * zone.size.y)
		var right_down = dungeon.map_to_world(zone.position + zone.size )
		var right_up = dungeon.map_to_world(zone.position + Vector2.RIGHT * zone.size.x)
		var color = Color.white
		draw_line(left_up, left_down, color)
		draw_line(left_down, right_down, color)
		draw_line(right_down, right_up, color)
		draw_line(right_up, left_up, color)
	if DRAW_ROOMS:
		for r in dungeon.rects:
			var left_up = dungeon.map_to_world(r.position)
			var left_down = dungeon.map_to_world(r.position + Vector2.DOWN * r.size.y)
			var right_down = dungeon.map_to_world(r.position + r.size )
			var right_up = dungeon.map_to_world(r.position + Vector2.RIGHT * r.size.x)
			var color = Color.blue
			draw_line(left_up, left_down, color)
			draw_line(left_down, right_down, color)
			draw_line(right_down, right_up, color)
			draw_line(right_up, left_up, color)




















