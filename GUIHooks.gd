extends Control

export var dungeon_node_path:NodePath
export var dungeon_drawer_node_path:NodePath
export var camera_node_path:NodePath

export var zone = Rect2(0, 0, 100, 55)
export var min_room_size = 3
export var big_rooms_amount =  Vector2(3, 5)
export var big_rooms_size =  Vector2(25, 30)
export var small_rooms_amount =  Vector2(10, 25)
export var small_rooms_size =  Vector2(8, 15)
export var hallways_helper = Vector2(20, 20)
export var hallways_size = 6

const MIN_VALUE = 6

onready var dungeon:Dungeon = get_node(dungeon_node_path)
onready var dungeon_drawer:Dungeon = get_node(dungeon_drawer_node_path)
onready var camera:Camera2D = get_node(camera_node_path)

func _ready() -> void:
	$Generate.connect("button_down", self, "start_generation")
	
	$Dungeon/Size/X.value = zone.size.x
	$Dungeon/Size/X.connect("value_changed", self, "set_dungeon_width")
	$Dungeon/Size/Y.value = zone.size.y
	$Dungeon/Size/Y.connect("value_changed", self, "set_dungeon_height")
	
	$BigRooms/Size/X.min_value = MIN_VALUE
	$BigRooms/Size/Y.min_value = MIN_VALUE
	
	$BigRooms/Size/X.value = big_rooms_size.x
	$BigRooms/Size/X.connect("value_changed", self, "set_big_rooms_size_x")
	$BigRooms/Size/Y.value = big_rooms_size.y
	$BigRooms/Size/Y.connect("value_changed", self, "set_big_rooms_size_y")
	$BigRooms/Amount/X.value = big_rooms_amount.x
	$BigRooms/Amount/X.connect("value_changed", self, "set_big_rooms_min")
	$BigRooms/Amount/Y.value = big_rooms_amount.y
	$BigRooms/Amount/Y.connect("value_changed", self, "set_big_rooms_max")
	
	$SmallRooms/Size/X.min_value = MIN_VALUE
	$SmallRooms/Size/Y.min_value = MIN_VALUE
	
	$SmallRooms/Size/X.value = small_rooms_size.x
	$SmallRooms/Size/X.connect("value_changed", self, "set_small_rooms_size_x")
	$SmallRooms/Size/Y.value = small_rooms_size.y
	$SmallRooms/Size/Y.connect("value_changed", self, "set_small_rooms_size_y")
	$SmallRooms/Amount/X.value = small_rooms_amount.x
	$SmallRooms/Amount/X.connect("value_changed", self, "set_small_rooms_min")
	$SmallRooms/Amount/Y.value = small_rooms_amount.y
	$SmallRooms/Amount/Y.connect("value_changed", self, "set_small_rooms_max")
	
	dungeon_drawer.dungeon = dungeon
	dungeon_drawer.zone = zone
	$Draw.pressed = dungeon_drawer.DRAW
	$Draw.connect("toggled", self, "set_draw")
	$Draw/Limits.pressed = dungeon_drawer.DRAW_ZONE
	$Draw/Limits.connect("toggled", self, "set_draw_zone")
	$Draw/Grid.pressed = dungeon_drawer.DRAW_GRID
	$Draw/Grid.connect("toggled", self, "set_draw_grid")
	$Draw/Rooms.pressed = dungeon_drawer.DRAW_ROOMS
	$Draw/Rooms.connect("toggled", self, "set_draw_rooms")

func _process(delta: float) -> void:
	var half_size = zone.size * dungeon.cell_size * 0.5
	camera.position = zone.position + half_size + Vector2(1400, 300)
	camera.zoom = Vector2.ONE * 6
	$ScrollContainer/VBoxContainer/Log.text = dungeon.gen_log

func start_generation() -> void:
	dungeon.start_generation(zone, min_room_size, big_rooms_amount, big_rooms_size,
	small_rooms_amount, small_rooms_size, hallways_helper, hallways_size)

func set_draw(value:bool) -> void:
	dungeon_drawer.DRAW = value
func set_draw_grid(value:bool) -> void:
	dungeon_drawer.DRAW_GRID = value
func set_draw_zone(value:bool) -> void:
	dungeon_drawer.DRAW_ZONE = value
func set_draw_rooms(value:bool) -> void:
	dungeon_drawer.DRAW_ROOMS = value

func set_small_rooms_size_x(value:float) -> void:
	small_rooms_size.x = value
func set_small_rooms_size_y(value:float) -> void:
	small_rooms_size.y = value
func set_small_rooms_min(value:float) -> void:
	small_rooms_size.x = value
func set_small_rooms_max(value:float) -> void:
	small_rooms_amount.y = value

func set_big_rooms_size_x(value:float) -> void:
	big_rooms_size.x = value
func set_big_rooms_size_y(value:float) -> void:
	big_rooms_size.y = value
func set_big_rooms_min(value:float) -> void:
	small_rooms_amount.x = value
func set_big_rooms_max(value:float) -> void:
	small_rooms_amount.y = value

func set_dungeon_width(value:float) -> void:
	zone.size.x = value
	dungeon_drawer.zone = zone
func set_dungeon_height(value:float) -> void:
	zone.size.y = value
	dungeon_drawer.zone = zone