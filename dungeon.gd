extends TileMap
class_name Dungeon

const EMPTY_TILE = -1

var rects:Array = []
var clusters:Array = []
var yield_generate
var yield_aux

var zone:Rect2
var gen_log:String

func is_generating() -> bool:
	return yield_generate != null

func start_generation(ZONE = Rect2(0, 0, 100, 55),
MIN_ROOM_SIZE = 3,
BIG_ROOMS_AMOUNT =  Vector2(3, 5), BIG_ROOMS_SIZE =  Vector2(25, 30),
SMALL_ROOMS_AMOUNT =  Vector2(10, 25), SMALL_ROOMS_SIZE =  Vector2(8, 15),
HALLWAYS_HELPER = Vector2(20, 20), HALLWAYS_SIZE = 6) -> void:
	if yield_generate != null:
		return
	randomize()
	clear()
	rects.clear()
	clusters.clear()
	yield_generate = null
	yield_aux = null
	yield_generate = generate(ZONE, MIN_ROOM_SIZE, BIG_ROOMS_AMOUNT, BIG_ROOMS_SIZE, SMALL_ROOMS_AMOUNT, SMALL_ROOMS_SIZE, HALLWAYS_HELPER, HALLWAYS_SIZE)

func _process(delta):
	if yield_generate != null:
		yield_generate = yield_generate.resume()

#########################################################
############### GENERATION
#########################################################

func generate(ZONE = Rect2(0, 0, 100, 55),
MIN_ROOM_SIZE = 3,
BIG_ROOMS_AMOUNT =  Vector2(3, 5), BIG_ROOMS_SIZE =  Vector2(25, 30),
SMALL_ROOMS_AMOUNT =  Vector2(10, 25), SMALL_ROOMS_SIZE =  Vector2(8, 15),
HALLWAYS_HELPER = Vector2(20, 20), HALLWAYS_SIZE = 6):
	zone = ZONE
	gen_log = ""
	gen_log += ("dungeon generation started\n")
	
	var margin = 0
	
	gen_log += ("placing rects (big rooms)\n")
	var amount = BIG_ROOMS_AMOUNT
	var size = BIG_ROOMS_SIZE
	yield_aux = generate_mutliple_rects(ZONE, int(ceil(rand_range(amount.x, amount.y))), margin, size.x, size.y, size.x, size.y)
	while yield_aux != null:
		yield_aux = yield_aux.resume()
		yield()
	
	gen_log += ("placing rects (small rooms)\n")
	amount = SMALL_ROOMS_AMOUNT
	size = SMALL_ROOMS_SIZE
	yield_aux = generate_mutliple_rects(ZONE, int(ceil(rand_range(amount.x, amount.y))), margin, size.x, size.y, size.x, size.y)
	while yield_aux != null:
		yield_aux = yield_aux.resume()
		yield()
	
#	clusters_init()
#	gen_log += ("found clusters: " + str(clusters.size()) + "\n")
	
	gen_log += ("placing rects (hallways for eliminating clusters)\n")
	yield_aux = generate_hallways_for_connecting_clusters(ZONE, MIN_ROOM_SIZE, margin)
	while yield_aux != null:
		yield_aux = yield_aux.resume()
		yield()

	gen_log += ("placing rects (hallways for connecting big rooms with all near rooms)\n")
	yield_aux = generate_hallways_for_big_rooms(ZONE, MIN_ROOM_SIZE, margin, HALLWAYS_HELPER, HALLWAYS_SIZE)
	while yield_aux != null:
		yield_aux = yield_aux.resume()
		yield()
	
	# clear tile map
	clear()
	
	gen_log += ("placing tiles: ground\n")
	place_tiles_ground(1)
	
	gen_log += ("placing tiles: walls\n")
#	place_tiles_walls(2)
	place_tiles_walls_clustered(ZONE, 2)
	
	clusters_init()
	gen_log += ("dungeon generation finished | rects: " + str(rects.size()) + " | clusters: " + str(clusters.size()) + "\n")

func generate_mutliple_rects(ZONE:Rect2, amount:int, margin:int, size_x_min:int, size_x_max:int, size_y_min:int, size_x_max:int) -> void:
	var count = 0
	var tries = 0
	var tries_max = amount * 100
	while count < amount:
		var rect = rand_rect(ZONE, size_x_min, size_x_max, size_y_min, size_x_max)
		rect = rects_resize_to_fit_complex(rect, margin, size_x_min, size_y_min)
		if rect != null:
			rects.append(rect)
			count += 1
			yield()
		tries += 1
		if tries > tries_max:
			gen_log += ("stopped generation of multiple rects, too many tries: " + str(tries) + "\n")
			break

func generate_hallways_for_connecting_clusters(ZONE:Rect2, MIN_ROOM_SIZE:int, margin:int):
	# this spaghetti, grabs a rect from rects,
	# checks if there are rooms to the right and bottom that can connect to
	# makes a room between them
	var appended = true
	while appended:
		yield()
		appended = false
		clusters_init()
		var tried = []
		while !appended:
			if tried.size() >= rects.size():
				break
			var i = randi() % rects.size()
			while tried.has(i):
				i = randi() % rects.size()
			tried.append(i)
			var r = rects[i]
			var g = ZONE.size.length()
			var do = randi() % 4
			if do == 0: # HALLWAYS TO LEFT
				var rg = r.grow_individual(g, 0, 0, 0)
				var rects2 = rects_get_rects_in_rect(rg, [r])
				for r2 in rects2:
					if appended:
						break
					if clusters_rects_are_on_same(r, r2):
						continue
					var clip = r2.clip(rg)
					if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
						var h = 3
						if clip.size.y >= 5 and r.size.y >= 15 and r2.size.y >= 15:
							h = 5
						elif clip.size.y >= 4 and r.size.y >= 10 and r2.size.y >= 10:
							h = 4
						var y = ceil(rand_range(clip.position.y, clip.position.y + clip.size.y - h))
						var x = r2.position.x + r2.size.x
						var w = abs(r.position.x - x)
						var hallway = Rect2(x, y, w, h)
						if !rects_intersects(hallway, margin):
							rects.append(hallway)
							appended = true
			if do == 1: # HALLWAYS TO TOP
				var rg = r.grow_individual(0, g, 0, 0)
				var rects2 = rects_get_rects_in_rect(rg, [r])
				for r2 in rects2:
					if appended:
						break
					if clusters_rects_are_on_same(r, r2):
						continue
					var clip = r2.clip(rg)
					if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
						var w = 3
						if clip.size.x >= 5 and r.size.x >= 15 and r2.size.x >= 15:
							w = 5
						elif clip.size.x >= 4 and r.size.x >= 10 and r2.size.x >= 10:
							w = 4
						var x = ceil(rand_range(clip.position.x, clip.position.x + clip.size.x - w))
						var y = r2.position.y + r2.size.y
						var h = abs(r.position.y - y)
						var hallway = Rect2(x, y, w, h)
						if !rects_intersects(hallway, margin):
							rects.append(hallway)
							appended = true
			if do == 2: # HALLWAYS TO RIGHT
				var rg = r.grow_individual(0, 0, g, 0)
				var rects2 = rects_get_rects_in_rect(rg, [r])
				for r2 in rects2:
					if appended:
						break
					if clusters_rects_are_on_same(r, r2):
						continue
					var clip = r2.clip(rg)
					if clip.size.x >= 3 and clip.size.y >= 3:
						var h = 3
						if clip.size.y >= 5 and r.size.y >= 15 and r2.size.y >= 15:
							h = 5
						elif clip.size.y >= 4 and r.size.y >= 10 and r2.size.y >= 10:
							h = 4
						var y = ceil(rand_range(clip.position.y, clip.position.y + clip.size.y - h))
						var x = r.position.x + r.size.x
						var w = abs(r2.position.x - x)
						var hallway = Rect2(x, y, w, h)
						if !rects_intersects(hallway, margin):
							rects.append(hallway)
							appended = true
			if do == 3: # HALLWAYS TO BOTTOM
				var rg = r.grow_individual(0, 0, 0, g)
				var rects2 = rects_get_rects_in_rect(rg, [r])
				for r2 in rects2:
					if appended:
						break
					if clusters_rects_are_on_same(r, r2):
						continue
					var clip = r2.clip(rg)
					if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
	#					var w = ceil(rand_range(3, clip.size.x))
						var w = 3
						if clip.size.x >= 5 and r.size.x >= 15 and r2.size.x >= 15:
							w = 5
						elif clip.size.x >= 4 and r.size.x >= 10 and r2.size.x >= 10:
							w = 4
						var x = ceil(rand_range(clip.position.x, clip.position.x + clip.size.x - w))
						var y = r.position.y + r.size.y
						var h = abs(r2.position.y - y)
						var hallway = Rect2(x, y, w, h)
						if !rects_intersects(hallway, margin):
							rects.append(hallway)
							appended = true

func generate_hallways_for_big_rooms(ZONE, MIN_ROOM_SIZE, margin:int, big_rooms_size:Vector2, min_size:float):
	var big_rects = []
	for r in rects:
		if r.size.x >= big_rooms_size.x and r.size.y >= big_rooms_size.y:
			big_rects.append(r)
	for r in big_rects:
		var max_tries = 1
		var g = ZONE.size.length()
		# LEFT
		var rg = r.grow_individual(g, 0, 0, 0)
		var rects2 = rects_get_rects_in_rect(rg, [r])
		for r2 in rects2:
			if r2.size.x < min_size or r2.size.y < min_size:
				continue
			var clip = r2.clip(rg)
			if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
				var count_tries = 0
				while count_tries < max_tries:
					var h = 3
					if clip.size.y >= 5 and r.size.y >= 15 and r2.size.y >= 15:
						h = 5
					elif clip.size.y >= 4 and r.size.y >= 10 and r2.size.y >= 10:
						h = 4
					var y = ceil(rand_range(clip.position.y, clip.position.y + clip.size.y - h))
					var x = r2.position.x + r2.size.x
					var w = abs(r.position.x - x)
					var hallway = Rect2(x, y, w, h)
					if !rects_intersects(hallway, margin):
						rects.append(hallway)
						break
					count_tries += 1
		# TOP
		rg = r.grow_individual(0, g, 0, 0)
		rects2 = rects_get_rects_in_rect(rg, [r])
		for r2 in rects2:
			if r2.size.x < min_size or r2.size.y < min_size:
				continue
			var clip = r2.clip(rg)
			if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
				var count_tries = 0
				while count_tries < max_tries:
					var w = 3
					if clip.size.x >= 5 and r.size.x >= 15 and r2.size.x >= 15:
						w = 5
					elif clip.size.x >= 4 and r.size.x >= 10 and r2.size.x >= 10:
						w = 4
					var x = ceil(rand_range(clip.position.x, clip.position.x + clip.size.x - w))
					var y = r2.position.y + r2.size.y
					var h = abs(r.position.y - y)
					var hallway = Rect2(x, y, w, h)
					if !rects_intersects(hallway, margin):
						rects.append(hallway)
						break
					count_tries += 1
		# LEFT
		rg = r.grow_individual(0, 0, g, 0)
		rects2 = rects_get_rects_in_rect(rg, [r])
		for r2 in rects2:
			if r2.size.x < min_size or r2.size.y < min_size:
				continue
			var clip = r2.clip(rg)
			if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
				var count_tries = 0
				while count_tries < max_tries:
					var h = 3
					if clip.size.y >= 5 and r.size.y >= 15 and r2.size.y >= 15:
						h = 5
					elif clip.size.y >= 4 and r.size.y >= 10 and r2.size.y >= 10:
						h = 4
					var y = ceil(rand_range(clip.position.y, clip.position.y + clip.size.y - h))
					var x = r.position.x + r.size.x
					var w = abs(r2.position.x - x)
					var hallway = Rect2(x, y, w, h)
					if !rects_intersects(hallway, margin):
						rects.append(hallway)
						break
					count_tries += 1
		# BOTTOM
		rg = r.grow_individual(0, 0, 0, g)
		rects2 = rects_get_rects_in_rect(rg, [r])
		for r2 in rects2:
			if r2.size.x < min_size or r2.size.y < min_size:
				continue
			var clip = r2.clip(rg)
			if clip.size.x >= MIN_ROOM_SIZE and clip.size.y >= MIN_ROOM_SIZE:
				var count_tries = 0
				while count_tries < max_tries:
					var w = 3
					if clip.size.x >= 5 and r.size.x >= 15 and r2.size.x >= 15:
						w = 5
					elif clip.size.x >= 4 and r.size.x >= 10 and r2.size.x >= 10:
						w = 4
					var x = ceil(rand_range(clip.position.x, clip.position.x + clip.size.x - w))
					var y = r.position.y + r.size.y
					var h = abs(r2.position.y - y)
					var hallway = Rect2(x, y, w, h)
					if !rects_intersects(hallway, margin):
						rects.append(hallway)
						break
					count_tries += 1

#########################################################
############### HELPERS
#########################################################

func rand_rect(zone:Rect2, size_x_min:int, size_x_max:int, size_y_min:int, size_y_max:int) -> Rect2:
	var size_x = round(rand_range(size_x_min, size_x_max))
	var size_y = round(rand_range(size_y_min, size_y_max))
	var pos_x = round(rand_range(zone.position.x, zone.position.x + zone.size.x - size_x))
	var pos_y = round(rand_range(zone.position.y, zone.position.y + zone.size.y - size_y))
	return Rect2(pos_x, pos_y, size_x, size_y)

#########################################################
############### RECTS
#########################################################

func rects_resize_to_fit(rect:Rect2, margin:int, min_size_x:int, min_size_y:int):
	if !rects_intersects(rect, margin):
		return rect
	while true:
		rect = rect.grow(-1)
		if rect.size.x < min_size_x or rect.size.y < min_size_y:
			return null
		if !rects_intersects(rect, margin):
			return rect
	return null

func rects_resize_to_fit_complex(rect:Rect2, margin:int, min_size_x:int, min_size_y:int):
	var rect_h = rect
	var rect_v = rect
	var rect_hv = rect
	var r = []
	# SHRINK HORIZONTALLY
	while rect_h != null:
		rect_h = rect_h.grow_individual(-1, 0, -1, 0)
		if rect_h.size.x < min_size_x or rect_h.size.y < min_size_y:
			rect_h = null
		elif !rects_intersects(rect_h, margin):
			r.append(rect_h)
			rect_h = null
	# SHRINK VERTICALLY
	while rect_v != null:
		rect_v = rect_v.grow_individual(0, -1, 0, -1)
		if rect_v.size.x < min_size_x or rect_v.size.y < min_size_y:
			rect_v = null
		elif !rects_intersects(rect_v, margin):
			r.append(rect_v)
			rect_v = null
	# SHINK HORIZONTALLY AND VERTICALLY
	while rect_hv != null:
		rect_hv = rect_hv.grow(-1)
		if rect_hv.size.x < min_size_x or rect_hv.size.y < min_size_y:
			rect_hv = null
		elif !rects_intersects(rect_hv, margin):
			r.append(rect_hv)
			rect_hv = null
	# PIPI
	if r.size() > 0:
		return r[randi() % r.size()]
	return null # no space there

func rects_has_point(point:Vector2, exclude:Array = []) -> bool:
	for r in rects:
		if !exclude.has(r) and r.has_point(point):
			return true
	return false

func rects_intersects(rect:Rect2, margin:int, exclude:Array = []) -> bool:
	rect = rect.grow(margin)
	for r in rects:
		if !exclude.has(r) and r.intersects(rect):
			return true
	return false

func rects_get_neighbours(rect:Rect2) -> Array:
	var neighbours = []
	for r in rects:
		if r != rect and (rect.grow_individual(1, -2, 1, -2).intersects(r) or rect.grow_individual(-2, 1, -2, 1).intersects(r)):
			neighbours.append(r)
	return neighbours

func rects_get_rects_in_rect(rect:Rect2, exclude:Array = []) -> Array:
	var rs = []
	for r in rects:
		if !exclude.has(r) and rect.intersects(r):
			rs.append(r)
	return rs

#########################################################
############### CLUSTER
#########################################################

func clusters_rects_are_on_same(rect1:Rect2, rect2:Rect2) -> bool:
	for cluster in clusters:
		if cluster.has(rect1) and cluster.has(rect2):
			return true
	return false

func clusters_erase_by_size(min_size:int = 1):
	var delete = []
	for cluster in clusters:
		if cluster.size() > min_size:
			continue
		for rect in cluster:
			rects.erase(rect)
			delete.append(cluster)
	while delete.size() > 0:
		clusters.erase(delete.pop_front())

func clusters_init() -> void:
	clusters.clear()
	for rect in rects:
		var neighbours = rects_get_neighbours(rect)
		if neighbours.size() == 0: # if the rect has no neighbours, its is own cluster
			clusters.append([rect])
			continue
		# check if the rect already is in a cluster
		var is_in_cluster = false
		for cluster in clusters:
			for r in cluster:
				if rect == r:
					is_in_cluster = true
			if is_in_cluster:
				break
		if is_in_cluster:
			continue
		# to avoid recursion we do this, since gdscript has problems with it
		# basically, searchs all the neighbours of the current rect's neighbours
		var cluster = [rect]
		while neighbours.size() > 0:
			var r = neighbours.pop_front()
			if !cluster.has(r):
				cluster.append(r)
			var ns = rects_get_neighbours(r)
			for n in ns:
				if !cluster.has(n):
					neighbours.append(n)
		clusters.append(cluster)

#########################################################
############### TILES
#########################################################

func place_tiles_ground(tile_index:int):
	for rect in rects:
		for y in rect.size.y:
			for x in rect.size.x:
				set_cell(rect.position.x + x, rect.position.y + y, tile_index)

func place_tiles_walls(tile_index:int):
	for rect in rects:
		for x in rect.size.x:
			set_cell(rect.position.x + x, rect.position.y + 0, tile_index)
			set_cell(rect.position.x + x, rect.position.y + rect.size.y - 1, tile_index)
		for y in rect.size.y:
			set_cell(rect.position.x + 0, rect.position.y + y, tile_index)
			set_cell(rect.position.x + rect.size.x - 1, rect.position.y + y, tile_index)

func place_tiles_walls_clustered(zone:Rect2, tile_index:int):
	for y in range(0, zone.size.y):
		for x in range(0, zone.size.x):
			var xx = zone.position.x + x
			var yy = zone.position.y + y
			if get_cell(xx, yy) != EMPTY_TILE:
				var left = get_cell(xx - 1, yy) == EMPTY_TILE
				var right = get_cell(xx + 1, yy) == EMPTY_TILE
				var up = get_cell(xx, yy - 1) == EMPTY_TILE
				var down = get_cell(xx, yy + 1) == EMPTY_TILE
				var up_left = get_cell(xx - 1, yy - 1) == EMPTY_TILE
				var up_right = get_cell(xx + 1, yy - 1) == EMPTY_TILE
				var down_left = get_cell(xx - 1, yy + 1) == EMPTY_TILE
				var down_right = get_cell(xx + 1, yy + 1) == EMPTY_TILE
				if up_left or up_right or down_left or down_right or left or right or up or down:
					set_cell(xx, yy, tile_index)




















