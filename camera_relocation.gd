extends Node2D

func _ready():
	var dungeon = get_node("../../Dungeon")
	get_node("../").zoom = dungeon.ZONE.size.length() / 35
	get_node("../").position = dungeon.ZONE.position + dungeon.ZONE.size / 2
	get_node("../").position *= dungeon.cell_size
#	get_node("../").position = dungeon.ZONE.position
#	get_node("../").position = dungeon.ZONE.end * dungeon.cell_size